FROM ubuntu:focal
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN printf "\n\033[0;36mInstalling packages...\033[0m\n" \
    && apt-get update \
    && apt-get install -y \
        bash \
        wget \
        curl \
        findutils \
        ffmpeg \
        mediainfo \
        gifsicle \
        imagemagick \
        webp \
        mediainfo \
        file \
    # DotNet
    && wget https://packages.microsoft.com/config/ubuntu/20.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
    && dpkg -i packages-microsoft-prod.deb \
    && rm packages-microsoft-prod.deb \
    && apt-get update \
    && apt-get install -y apt-transport-https \
    && apt-get update \
    && apt-get install -y dotnet-sdk-5.0 \
    # Creating version info files
    && printf "\n\033[0;36mCreating version info files...\033[0m\n" \
    && mkdir -p /info \
    && cat /etc/os-release | grep VERSION_ID | cut -d "=" -f 2 | cut -d "\"" -f 2 > /info/ubuntu \
    && ffmpeg -version | head -1 | cut -d " " -f 3 > /info/ffmpeg \
    && chmod -R 0777 /info \
    # Preparing mounts
    && printf "\n\033[0;36mPreparing mounts...\033[0m\n" \
    mkdir -p /input \
    mkdir -p /output \
    mkdir -p /in-progress \
    chmod -R 0777 /input /output \
    # Cleanup
    && apt-get clean all \
    && rm -rf /var/lib/apt/lists/* \
    # Done
    && printf "\n\033[0;36mDone.\033[0m\n"

VOLUME [ "/input", "/output", "/in-progress" ]

RUN [ "/bin/bash" ]